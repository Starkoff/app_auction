module ApplicationHelper
  private
  def authorise
    rem_token=User.encrypt(cookies[:remember_token])
    unless User.find_by(remember_token: rem_token)
      session[:return_to] = request.url if request.get?
      redirect_to new_session_url, notice: "Please Login"
    end
  end
 
  def authoriseout
    rem_token = User.encrypt(cookies[:remember_token])
    unless !User.find_by(remember_token: rem_token)
      session[:return_to] = request.url if request.get?
      redirect_to root_url, notice: "Please Login"
    end
  end
end