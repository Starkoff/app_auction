class ItemsController < ApplicationController
  include ApplicationHelper
  include SessionsHelper
  before_action :set_item, only: [:show, :edit, :update, :destroy]
  before_action :authorise, only: [:new]
  before_action :current_user, only: [:new, :create, :edit, :update, :destroy, :show]
  
  
  # GET /items
  # GET /items.json
  def index
    #setting the parameters for the Item.search
    @items = Item.search(params[:query], params[:searchItems])
    @categoryOpts = category_options
  end

  # GET /items/1
  # GET /items/1.json
  def show
    #category is getting the items.category_id
    @category = Category.find(@item.category_id)
    @bid = Bid.new
    @bids = @item.bids
  end

  # GET /items/new
  def new
    @item = Item.new(:user_id => @user.id)
  end

  # GET /items/1/edit
  def edit
    #category is getting item the catgegories name aswell as id value
    @category = Category.find(@item.category_id).name
  end

  # POST /items
  # POST /items.json
  def create
    @item = Item.new(item_params)
    @item.update(vendor_id: @current_user.id)

    respond_to do |format|
      if @item.save
        format.html { redirect_to @item, notice: 'Item was successfully created.' }
        format.json { render action: 'show', status: :created, location: @item }
      else
        format.html { render action: 'new' }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /items/1
  # PATCH/PUT /items/1.json
  def update
    respond_to do |format|
      if @item.update(item_params)
        format.html { redirect_to @item, notice: 'Item was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /items/1
  # DELETE /items/1.json
  def destroy
    @item.destroy
    respond_to do |format|
      format.html { redirect_to items_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_item
      @item = Item.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def item_params
      params.require(:item).permit(:name, :description, :category_id, :vendor, :starting_price)
    end
    
  def category_options 
    #collecting everything from category inclusive of its name and id
    Category.all.collect { |c| [c.name, c.id] } 
  end
end
