class UsersController < ApplicationController
  include SessionsHelper
  include ApplicationHelper
  before_action :authoriseout, only: [:new]
  before_action :authorise, only: [:index, :edit, :update, :destroy, :show]
  before_action :current_user, only: [:index, :new, :create, :edit, :update, :destroy, :show]
  
  def new
    @user = User.new
  end
  
  def index
    @users = User.all
  end
  
  def show
    @user = User.find(params[:id])
  end
  

  
  def create
    @user = User.new(user_params)
    if @user
      sign_in @user
      flash[:success] = "Welcome to Stark Auction Industries! "
      redirect_to @user
    else
      render 'new'
    end
  end
  
  def destroy
    @user = User.find(params[:id])
    #this is to reflect my passion for big balls :D
    @user.destroy
    redirect_to session[:return_to] || root_url
    session[:return_to] = nil
  end
  
  private
  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end
end

