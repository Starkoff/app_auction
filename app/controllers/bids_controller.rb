class BidsController < ApplicationController
  
  def create 
    @current_user = User.find(params[:bid][:user_id])
    @item = Item.find(params[:bid][:item_id])
  
  if @current_user.id != @item.vendor_id
    @bid = Bid.new(bid_params)
    if @bid.save
      @item.update(price: @bid.amount)
      flash[:notice] = "New Bid Added"
      redirect_to @item, notice: "You are the highest bidder!"
      @item.starting_price = @bid.amount
    else
      redirect_to @item, notice: "You cannot bid on your own items!"
    end
  end
end

def bid_params
  params.require(:bid).permit(:amount, :user_id, :item_id)
end

def destroy
end

def show
  @id = params[:id]
  @item = Item.find(params[:id])
  @users = User.find(@item.vendor_id)
  @bids = Bid.search(@item.id,nil)
end
end
