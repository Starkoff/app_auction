class Item < ActiveRecord::Base
  has_many :bids, dependent: :destroy, :foreign_key => :item_id
  belongs_to :category
  validates :name, presence: true
  validates :category, presence: true
  validates :starting_price, presence: true
  

  def self.search(query,searchObjects)
    if query == "" && searchObjects == "" || searchObjects == nil
     find(:all)
    elsif query != "" && searchObjects != "" 
      find(:all, conditions: ['name like ? AND category_id = ? OR description like ? AND category_id = ? ', "%#{query}%", searchObjects, "%#{query}%", searchObjects])
    elsif query != "" && searchObjects == ""
     find(:all, conditions: ['name like ? OR description like ?', "%#{query}%", "%#{query}%"])  
    elsif query == "" && searchObjects != ""
     find(:all, conditions: ['category_id = ?', searchObjects]) 
    else
      find(:all)
    end
  end
end