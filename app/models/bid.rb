class Bid < ActiveRecord::Base
  belongs_to :user
  belongs_to :item
  validates :user_id, presence: true
  validates :item_id, presence: true
  default_scope order ("created_at DESC")
  validates :amount, :numericality => {:greater_than => 1, :less_than => 9999999}
  
  def self.search(item, user)
    if item != nil && user != nil
      find(:all, conditions: ['item_id = ? AND user_id = ?', item, user])
    elsif !item && user
      find(:all, conditions: ['user_id = ?', user])
    else
      find(:all, conditions: ['item_id = ?', item])
    end
  end
end
