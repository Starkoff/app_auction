class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :name
      t.string :description
      t.string :vendor
      t.integer :category_id
      t.float :starting_price

      t.timestamps
    end
  end
end