class CreateBids < ActiveRecord::Migration
  def change
    create_table :bids do |t|
      t.integer :user_id
      t.integer :item_id
      t.decimal :amount
      t.datetime :created_at
      t.datetime :updated_at
      
      t.timestamps
    end
  end
end
