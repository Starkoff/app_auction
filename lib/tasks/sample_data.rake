namespace :db do
  desc "Fill database with sample data"
  task populate: :environment do
    # Explicit category definitions
    Category.create!(name: "Books", description: "readable items")
    Category.create!(name: "Movies", description: "watchable items")
    User.create!(name: "admin", password: "password", password_confirmation: "password", email: "admin@email.com", admin: true)
    User.create!(name: "user1", password: "password", password_confirmation: "password", email: "user1@email.com", admin: false)
    User.create!(name: "user2", password: "password", password_confirmation: "password", email: "user2@email.com", admin: false)
    
    # Repetitive, implicit item definitions
    15.times do |n|
      name  = "Book-#{n+1}"
      vendor = "Vendor-#{n % 10}"
      category_id = n % 2 + 1
      description = "description"
      starting_price = n * 7
      Item.create!(name: name,
                   vendor: vendor,
                   category_id: category_id,
                   description: description,           
                   starting_price: starting_price)
    end
  end
end