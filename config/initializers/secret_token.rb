# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
AuctionApp::Application.config.secret_key_base = 'ca85c5a47affdb281f27ea04f4e35fa55cb87c383f46e3c43fafa4652cd6fe40c3416f3cf6e5b407d7ddd9b47ffb05624c4371867437708e666bbb56d622a639'
